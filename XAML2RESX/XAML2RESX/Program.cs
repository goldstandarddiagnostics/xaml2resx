﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace XAML2RESX
{
    [StructLayout(LayoutKind.Auto, CharSet = CharSet.Auto)]
    internal class Program
    {
        public static int Main(string[] args)
        {
            Console.WriteLine("XAML To Resource String Convertor");
            Console.WriteLine("Version: " + Assembly.GetEntryAssembly().GetName().Version.ToString());
            string currentDirectory = Directory.GetCurrentDirectory();
            if (Debugger.IsAttached)
                currentDirectory = args[0];
            if (Directory.Exists(currentDirectory))
            {
                Console.WriteLine(string.Format("Using \"{0}\" as folder...", (object)currentDirectory));
                Program.ScanFolder(currentDirectory, (string)null, (string)null);
            }
            Console.Write("Done...");
            Console.ReadLine();
            int num = 1;
            return num;
        }

        private static void ScanFolder(string aFolder, string aCurrentNameSpace, string aCurrentResxFileName)
        {
            if (aFolder.Contains(".hg"))
                return;
            string currentNameSpace = Program.CheckNameSpace(aFolder).Item2 ?? aCurrentNameSpace;
            string currentResxFile = Program.CheckNameSpace(aFolder).Item1 ?? aCurrentResxFileName;
            Program.ScanXAMLFiles(aFolder, currentNameSpace, currentResxFile);
            string[] directories = Directory.GetDirectories(aFolder);
            int index = 0;
            string[] strArray = directories;
            if (strArray == null)
                return;
            for (; index < strArray.Length; ++index)
                Program.ScanFolder(strArray[index], currentNameSpace, currentResxFile);
        }

        private static Tuple<string, string> CheckNameSpace(string aFolder)
        {
            string resxFile = (string)null;
            string nameSpc = (string)null;
            string[] directories = Directory.GetDirectories(aFolder, "Properties");
            foreach (var str in directories)
            {
                if (File.Exists(str + new string(Path.DirectorySeparatorChar, 1) + "Resources.Designer.cs")) // C#
                {
                    if (File.Exists(str + new string(Path.DirectorySeparatorChar, 1) + "Resources.resx"))
                        resxFile = str + new string(Path.DirectorySeparatorChar, 1) + "Resources.resx";
                    string[] lines = File.ReadAllLines(str + new string(Path.DirectorySeparatorChar, 1) + "Resources.Designer.cs");
                    foreach (var line in lines)
                    {
                        if (line.StartsWith("namespace"))
                        {
                            nameSpc = line.Replace("namespace ", "").Replace(" {", "");
                            Console.Write("Namespace: ");
                            Console.WriteLine(nameSpc);
                            break;
                        }
                    }
                }

                if (File.Exists(str + new string(Path.DirectorySeparatorChar, 1) + "Resources.Designer.pas")) // Oxygene
                {
                    if (File.Exists(str + new string(Path.DirectorySeparatorChar, 1) + "Resources.resx"))
                        resxFile = str + new string(Path.DirectorySeparatorChar, 1) + "Resources.resx";
                    string[] lines = File.ReadAllLines(str + new string(Path.DirectorySeparatorChar, 1) + "Resources.Designer.pas");
                    foreach (var line in lines)
                    {
                        if (line.StartsWith("namespace"))
                        {
                            nameSpc = line.Replace("namespace ", "").Replace(";", "");
                            Console.Write("Namespace: ");
                            Console.WriteLine(nameSpc);
                            break;
                        }
                    }
                }
            }
            return new Tuple<string, string>(resxFile, nameSpc);
        }

        private static void ScanXAMLFiles(string aFolder, string aNameSpace, string aResXFileName)
        {
            string[] files = Directory.GetFiles(aFolder, "*.xaml");
            string str = "xmlns:resx=\"clr-namespace:" + aNameSpace + "\"";
            foreach (var path in files)
            {
                bool flag = false;
                int num = -1;
                List<string> stringList = new List<string>((IEnumerable<string>)File.ReadAllLines(path));
                for (int i = 0; i < stringList.Count - 1; i++)
                {
                    if (stringList[i].Contains(str)) flag = true;
                    if ((num == -1) && (stringList[i].Contains("http://schemas.microsoft.com/winfx/2006/xaml/presentation")))
                    {
                        num = i;
                    }
                    stringList[i] = Program.ParseXAMLLine(stringList[i], " Content=\"", aResXFileName);
                    stringList[i] = Program.ParseXAMLLine(stringList[i], " Text=\"", aResXFileName);
                    stringList[i] = Program.ParseXAMLLine(stringList[i], " Header=\"", aResXFileName);
                    stringList[i] = Program.ParseXAMLLine(stringList[i], " Title=\"", aResXFileName);
                    stringList[i] = Program.ParseXAMLLine(stringList[i], " ToolTip=\"", aResXFileName);
                    stringList[i] = Program.ParseXAMLLine(stringList[i], " StringFormat=\"", aResXFileName);
                }

                if (!flag)
                    stringList.Insert(num + 1, "    " + str);
                File.WriteAllLines(path, (IEnumerable<string>)stringList, Encoding.UTF8);
            }
        }


        private static string GetInnerXMLValue(string aInnerXML)
        {
            string str = string.Empty;
            int startIndex = aInnerXML.IndexOf("<value>") + 7;
            int num = aInnerXML.IndexOf("</value>");
            if (num > startIndex)
                str = aInnerXML.Substring(startIndex, num - startIndex);
            return str;
        }

        private static string CreateResourceName(string aString, string aResxFileName)
        {
            string str = "str" + Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(Regex.Replace(aString, "[\\[\\]]", "sbrc"), "[\\(\\)]", "rbrc"), "[\\{\\}]", "cbrc"), ":", "cln"), "=", "eql"), "\\?", "qm"), ";", "sc"), ",", "cm"), "\\.\\.\\.\\.", "4dt"), "\\.\\.\\.", "3dt"), "\\.\\.", "2dt"), "\\.", "dt"), "-", "dsh"), "\\+", "pls"), "/", "slsh"), "\\\\", "bslsh"), "[^a-zA-Z0-9]", "_");
            XmlDocument xmlDocument = new XmlDocument();
            try
            {
                xmlDocument.Load(aResxFileName);
                XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("data");
                if (elementsByTagName != null && elementsByTagName != null)
                {
                    IEnumerator enumerator = elementsByTagName.GetEnumerator();
                    if (enumerator != null)
                    {
                        try
                        {
                            while (enumerator.MoveNext())
                            {
                                XmlNode current = enumerator.Current as XmlNode;
                                if (current != null && Program.GetInnerXMLValue(current.InnerXml).ToUpper() == aString.ToUpper())
                                    return current.Attributes[0].InnerText;
                            }
                        }
                        finally
                        {
                            if (enumerator is IDisposable)
                                (enumerator as IDisposable).Dispose();
                        }
                    }
                }
                XmlElement element1 = xmlDocument.CreateElement("data");
                XmlAttribute attribute1 = xmlDocument.CreateAttribute("name");
                attribute1.Value = str;
                element1.Attributes.Append(attribute1);
                XmlAttribute attribute2 = xmlDocument.CreateAttribute("xml:space");
                attribute2.Value = "preserve";
                element1.Attributes.Append(attribute2);
                XmlElement element2 = xmlDocument.CreateElement("value");
                element2.InnerText = aString;
                element1.AppendChild((XmlNode)element2);
                xmlDocument.DocumentElement.AppendChild((XmlNode)element1);
                xmlDocument.Save(aResxFileName);
            }
            finally
            {
                (xmlDocument as IDisposable)?.Dispose();
            }
            return str;
        }

        private static string ParseXAMLLine(string aLine, string aIdentifier, string aResxFileName)
        {
            string str1 = aLine;
            int num1 = 0;
            if (aLine.Contains(aIdentifier))
            {
                int startIndex = aLine.IndexOf(aIdentifier) + aIdentifier.Length;
                if ((aLine[startIndex] != '{' ? 0 : (aLine[startIndex + 1] != '}' ? 1 : 0)) != 0)
                    return str1;
                if ((aLine[startIndex] != '{' ? 0 : (aLine[startIndex + 1] == '}' ? 1 : 0)) != 0)
                    num1 = 2;
                int num2 = aLine.IndexOf('"', startIndex);
                if (num2 > startIndex + num1)
                {
                    string str2 = "{x:Static resx:Resources." + Program.CreateResourceName(aLine.Substring(startIndex + num1, num2 - (startIndex + num1)), aResxFileName) + "}";
                    aLine = aLine.Remove(startIndex, num2 - startIndex);
                    str1 = aLine.Insert(startIndex, str2);
                    Console.WriteLine(str1);
                }
            }
            return str1;
        }

    }
}

